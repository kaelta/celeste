# Celeste
A discord bot that lets users (somewhat) safely test code snippets and returns the output, play music in voice chat, and overall make servers a bit more fun.

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)

## Running Code through Celeste

the current way to run code is with celeste's `. compile!` command, followed by markdown code blocks to infer the language, ie:

`. compile! language`

```lua
-- your code here
```

(as one message)

![Screenshot 2021-07-29 at 17.17.18](doc/testcode.png)

## Logs

A full log of different functions that Celeste runs gets output to the terminal by default, but obvs you can just do `luvit bot.lua > log.txt` if you rly want more. otherwise there's a semi-working one for all the commands

## Adding languages

adding languages is rly easy, each language has its own file within `commands/langs/` and follows this format:

```lua
return function(...)
	t = select(1, ...)	-- the message content that gets received & put into a table
	local filename = 'commands/cache/code' .. math.floor(os.clock() * 420.69) .. ".ex"	-- for random filenames
	local code = io.open(filename, 'w+')
	for i = 2, #t do	-- start from two to ignore the language selector & write the code to a file
		code:write(t[i], "\n")
	end
	code:close()
	function os.capture(input)	-- like `os.execute`, but uses `assert!` & `io.popen` to get around pesky subprocess issues
	  local f = assert(io.popen(input, 'r'))
	  return assert(f:read('*a'))
	end
	print('running elixir code now!!')
	return os.capture("elixir "..filename)	-- output automatically gets send to your discord server!!
end
```

then in `celeste.lua` add your language inside the `Celeste.langfunctions` table, where each value should be a file within the `commands.langs` folder, like `commands.langs.javascript.lua`

```lua
Celeste.langfunctions = {
    ["elixir"] = 'commands.langs.elixir',
    ["lua"]    = 'commands.langs.lua',
    ["moon"]   = 'commands.langs.moon',
    ["python"] = 'commands.langs.python'
  ... and so on and so forth
  }
```
