local Celeste = require 'celeste'
return function(...)
	msg = select(1, ...)
	local str = ""
	for command, _ in pairs(Celeste.commands) do
		str = str .. "`" .. Celeste.prefix .. " " .. command .. "`" .. ", "
	end
	local langs = ""
	for lang, _ in pairs(Celeste.langfunctions) do
		langs = langs .. "`" .. lang .. "`" .. ", "
	end
	msg:reply {
		embed = {
			title = "here are your options ... ",
			description = "Celeste v " .. Celeste.vers,
			fields = {
				{
					name = "commands",
					value = str,
					inline = false
				},
				{
					name = "extra compiling help",
					value = "currently the celeste compiler supports ~\n" .. langs .. "\n" ..
					"compile like this: `. compile! ```language` and your code below",
					inline = false
				},
				{
					name = 'alsooo',
					value = 'my prefix is `' .. Celeste.prefix .. '`',
					inline = false
				},
			},
			footer = {
				text = "My git repo is here ~ [link](https://gitlab.com/cxssmira/celeste)"
			},
			color = 0x880088
		}
	}
end